package br.com.senac.ex6;

import br.com.rodrigo.Valores;
import br.com.rodrigo.CalcularDesconto;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestCalcularDesconto {

    @Test
    public void testDesconto() {
       
        Valores vl = new Valores(100, 5, 1000);
        double rest = CalcularDesconto.getPreco(vl);
        assertEquals(490, rest, 0.1);
    }
}
